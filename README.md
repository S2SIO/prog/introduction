# Introduction à la programmation

## Introduction
Nous poursuivrons la progression suivante :
* Pourquoi apprendre à programmer ?
* Qu'est-ce que la programmation ?
* Pourquoi choisir Python ?

Nous présenterons ensuite le **projet** :
Développer une calculette éco-déplacements, qui permet de calculer l'impact de
nos déplacements quotidiens sur l'environnement et vos dépenses.

Ce programme est inspiré de la calculette de l'[Ademe], qui en plus de ne pas
être un logiciel libre, n'est pas écrit en python.

## Missions

Le développement de notre calculette se fera en plusieurs étapes (itérations),
présentées sous forme de missions, qui permettront d'appréhender les différents
objectifs notionnels.

Nous nous concentrons sur les premières missions sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un programme GUI, présente de nombreux avantages,
ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps,
car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire
de fenêtres et autres composants graphiques.

### Mission n°1: afficher
Dans un premier temps, nous nous fixons pour objectif d'afficher l'en tête résumant le programme : « Calculette : Eco-déplacements. Calculez l'impact de vos déplacements quotidiens sur l'environnement et vos dépenses. ».

Puis nous essayerons d'afficher la distance entre le domicile et le travail. Enfin, nous afficherons les coûts (financier, effet de serre, énergétique) des différents modes de transports.

Voir le [Code source][afficher].

### Mission n°2: saisir
L'utilisateur de notre application doit maintenant pouvoir saisir la distance entre son domicile et son lieu de travail. Il doit également pouvoir choisir son mode de transport...

Puis nous essayerons de séparer les différentes parties de notre code : affichage de l'en tête, des coûts, saisie du déplacement, etc. Tout ceci se fait en créant des **fonctions**.
Voir le [Code source][saisir].

### Mission n°3: tester
Lors de nos missions précédentes, nous sommes parvenus à afficher notre
calculette et à demander à l'utilisateur de saisir la distance entre son
domicile et son travail ainsi que son mode de déplacement...
Mais nous n’avons pas pris en compte le mode de déplacement saisie,
nous avons considéré, choix raisonnable, que par défaut il prenait le train !
En effet, pour cela il nous manquait des connaissances :
la possibilité d'effectuer un test entre deux valeurs.

Voir le [Code source][tester].

### Mission n°4: optimiser
Maintenant que notre application remplit à minimum le cahier des charges,
nous allons essayer d'optimiser notre application, c'est à dire
d'optimiser le code, de rendre certains traitements moins gourmands en
ressource, d'améliorer le dialogue homme-machine, de mieux gérer les données.

Voir le [Code source][optimiser].

### Mission n°5: gérer les erreurs
Lorsque l'on développe un programme, il y a une partie essentielle à ne pas négliger :
la **gestion des erreurs**. On ne peut pas laisser l'utilisateur complètement
perdu face à un écran affichant un message incompréhensible...

Voir le [Code source][gerer].

## Travaux pratiques
Des exercices dirigés permettront d'approfondir les notions étudiées
durant les missions.


[Ademe]: https://www.ademe.fr/
[afficher]: https://framagit.org/ced/CEDy/1-afficher
[saisir]: https://framagit.org/ced/CEDy/2-saisir
[tester]: https://framagit.org/ced/CEDy/3-tester
[optimiser]: https://framagit.org/ced/CEDy/4-optimiser
[gerer]: https://framagit.org/ced/CEDy/5-gerer-les-erreurs
